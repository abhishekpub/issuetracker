﻿namespace IssueTracker.Domain.Enums
{
    public enum IssueType
    {
        Story,
        Bug,
        Task,
    }
}