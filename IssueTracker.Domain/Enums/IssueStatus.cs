﻿namespace IssueTracker.Domain.Enums
{
    public enum IssueStatus
    {
        ToDo,
        InProgress,
        Done
    }
}