﻿using System.ComponentModel.DataAnnotations;

namespace IssueTracker.Domain.Entities.Account
{
    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
