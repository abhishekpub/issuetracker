﻿using IssueTracker.Domain.Common;
using IssueTracker.Domain.Entities.Account;

namespace IssueTracker.Domain.Entities
{
    public class ProjectParticipant : AuditableBaseEntity
    {
        public string UserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
