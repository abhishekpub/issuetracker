﻿using System.Collections.Generic;
using IssueTracker.Domain.Common;

namespace IssueTracker.Domain.Entities
{
    public class Project : AuditableBaseEntity
    {
        public string Key { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<ProjectParticipant> ProjectParticipants { get; set; }
        public ICollection<Issue> Issues { get; set; }
    }
}