﻿using System.Collections.Generic;
using IssueTracker.Domain.Common;

namespace IssueTracker.Domain.Entities
{
    public class CustomField : AuditableBaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string DefaultValue { get; set; }

        public bool IsRequired { get; set; }

        public int IssueTypeId { get; set; }
        public IssueType IssueType { get; set; }

        public ICollection<CustomFieldValue> CustomFieldValues { get; set; }
    }
}
