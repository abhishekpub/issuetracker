﻿using System.Collections.Generic;
using IssueTracker.Domain.Common;

namespace IssueTracker.Domain.Entities
{
    public class IssueStatus : AuditableBaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Issue> Issues { get; set; }
    }
}
