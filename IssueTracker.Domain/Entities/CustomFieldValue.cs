﻿using IssueTracker.Domain.Common;

namespace IssueTracker.Domain.Entities
{
    public class CustomFieldValue : AuditableBaseEntity
    {
        public int IssueId { get; set; }
        public Issue Issue { get; set; }

        public int CustomFieldId { get; set; }
        public CustomField CustomField { get; set; }

        public string Value { get; set; }
    }
}
