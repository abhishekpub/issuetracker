﻿using System.Collections.Generic;
using IssueTracker.Domain.Common;

namespace IssueTracker.Domain.Entities
{
    public class Issue : AuditableBaseEntity
    {
        public int ProjectId { get; set; }

        public Project Project { get; set; }

        public string Key { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string AssignedTo { get; set; }

        public int IssueStatusId { get; set; }
        public IssueStatus Status { get; set; }

        public int IssueTypeId { get; set; }
        public IssueType IssueType { get; set; }

        public ICollection<CustomFieldValue> CustomFieldValues { get; set; }
    }
}
