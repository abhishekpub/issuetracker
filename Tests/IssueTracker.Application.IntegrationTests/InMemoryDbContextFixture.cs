﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IssueTracker.Application.Interfaces;
using IssueTracker.Domain.Entities.Account;
using IssueTracker.Infrastructure.Persistence.Contexts;
using IssueTracker.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Respawn;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Moq;

namespace IssueTracker.Application.IntegrationTests
{
    public class InMemoryDbContextFixture : IDisposable
    {
        private static IConfigurationRoot _configuration;
        private static IServiceScopeFactory _scopeFactory;
        private static Checkpoint _checkpoint;
        private static string _currentUserId;

        public InMemoryDbContextFixture()
        {
            //var serviceCollection = new ServiceCollection();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();

            var startup = new Startup(_configuration);
            var services = new ServiceCollection();

            ////RegisterFreshDbServices();
            //var applicationDbContext = services.FirstOrDefault(d =>
            //    d.ServiceType == typeof(IApplicationDbContext));

            //services.Remove(applicationDbContext);

            // Register testing version
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseInMemoryDatabase("IssueTrackerDb"));

            ServiceProvider = services.BuildServiceProvider();

            services.AddSingleton(Mock.Of<IWebHostEnvironment>(w =>
                w.EnvironmentName == "Development" &&
                w.ApplicationName == "IssueTracker.WebApi"));

            services.AddLogging();

            startup.ConfigureServices(services);

            //RegisterFreshAccountServices();
            // Replace service registration for ICurrentUserService
            // Remove existing registration
            var authenticatedUserServiceDescriptor = services.FirstOrDefault(d =>
                d.ServiceType == typeof(IAuthenticatedUserService));

            services.Remove(authenticatedUserServiceDescriptor);

            // Register testing version
            services.AddTransient(provider =>
                Mock.Of<IAuthenticatedUserService>(s => s.UserId == _currentUserId));

            _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

            _checkpoint = new Checkpoint
            {
                TablesToIgnore = new[] { "__EFMigrationsHistory" }
            };

            EnsureDatabase();
        }

        //private void RegisterFreshDbServices()
        //{
        //    var applicationDbContext = services.FirstOrDefault(d =>
        //        d.ServiceType == typeof(IApplicationDbContext));

        //    services.Remove(applicationDbContext);

        //    // Register testing version
        //    services.AddDbContext<ApplicationDbContext>(options =>
        //        options.UseInMemoryDatabase("IssueTrackerDb"), ServiceLifetime.Transient);
        //}

        //private void RegisterFreshAccountServices()
        //{
        //    // Replace service registration for ICurrentUserService
        //    // Remove existing registration
        //    var authenticatedUserServiceDescriptor = services.FirstOrDefault(d =>
        //        d.ServiceType == typeof(IAuthenticatedUserService));

        //    services.Remove(authenticatedUserServiceDescriptor);

        //    // Register testing version
        //    services.AddTransient(provider =>
        //        Mock.Of<IAuthenticatedUserService>(s => s.UserId == _currentUserId));

        //}

        private static void EnsureDatabase()
        {
            using var scope = _scopeFactory.CreateScope();
            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
        }

        //public void ResetDatabase()
        //{
        //    RegisterFreshDbServices();
        //    RegisterFreshAccountServices();
        //}

        public async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            using var scope = _scopeFactory.CreateScope();

            var mediator = scope.ServiceProvider.GetService<IMediator>();

            return await mediator.Send(request);
        }

        public async Task<string> RunAsDefaultUserAsync()
        {
            return await RunAsUserAsync("test@local", "Testing1234!");
        }

        public async Task<string> RunAsUserAsync(string userName, string password)
        {
            using var scope = _scopeFactory.CreateScope();
            var userManager = scope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
            var existingUser = await userManager.Users.FirstOrDefaultAsync(x => x.UserName == userName);
            
            if (existingUser != null)
            {
                return existingUser.Id;
            }

            var user = new ApplicationUser { UserName = userName, Email = userName };
            var result = await userManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                _currentUserId = user.Id;
                //RegisterFreshAccountServices();
                return _currentUserId;
            }

            var errors = string.Join(Environment.NewLine, result.Errors);
            throw new Exception($"Unable to create {userName}.{Environment.NewLine}{errors}");
        }

        public async Task ResetState()
        {
            await _checkpoint.Reset(_configuration.GetConnectionString("DefaultConnection"));
            _currentUserId = null;
        }

        public async Task<TEntity> FindAsync<TEntity>(params object[] keyValues)
            where TEntity : class
        {
            using var scope = _scopeFactory.CreateScope();

            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            return await context.FindAsync<TEntity>(keyValues);
        }

        public async Task AddAsync<TEntity>(TEntity entity)
            where TEntity : class
        {
            using var scope = _scopeFactory.CreateScope();

            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            context.Add(entity);

            await context.SaveChangesAsync();
        }

        public ServiceProvider ServiceProvider { get; private set; }

        public void Dispose()
        {
            
        }
    }
}
