//using System;
//using System.IO;
//using System.Linq;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.DependencyInjection;
//using Moq;
//using Respawn;
//using Xunit;


//namespace IssueTracker.Application.IntegrationTests
//{
//    public class Testing : IClassFixture<>
//    {
//        private static IConfigurationRoot _configuration;
//        private static IServiceScopeFactory _scopeFactory;
//        private static Checkpoint _checkpoint;
//        private static string _currentUserId;

//        []
//        public void RunBeforeAnyTests()
//        {
//            var builder = new ConfigurationBuilder()
//                .SetBasePath(Directory.GetCurrentDirectory())
//                .AddJsonFile("appsettings.json", true, true)
//                .AddEnvironmentVariables();

//            _configuration = builder.Build();

//            var startup = new Startup(_configuration);

//            var services = new ServiceCollection();

//            services.AddSingleton(Mock.Of<IWebHostEnvironment>(w =>
//                w.EnvironmentName == "Development" &&
//                w.ApplicationName == "CleanArchitecture.WebUI"));

//            services.AddLogging();

//            startup.ConfigureServices(services);

//            // Replace service registration for ICurrentUserService
//            // Remove existing registration
//            var currentUserServiceDescriptor = services.FirstOrDefault(d =>
//                d.ServiceType == typeof(ICurrentUserService));

//            services.Remove(currentUserServiceDescriptor);

//            // Register testing version
//            services.AddTransient(provider =>
//                Mock.Of<ICurrentUserService>(s => s.UserId == _currentUserId));

//            _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

//            _checkpoint = new Checkpoint
//            {
//                TablesToIgnore = new[] { "__EFMigrationsHistory" }
//            };

//            EnsureDatabase();
//        }
//    }
//}
