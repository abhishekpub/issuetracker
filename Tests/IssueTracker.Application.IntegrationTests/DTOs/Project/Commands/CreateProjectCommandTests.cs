﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.IntegrationTests.DTOs.Project.Commands
{
    public class CreateProjectCommandTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;

        public CreateProjectCommandTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
            Task.Run(async ()=> await _fixture.ResetState());
        }

        [Fact]
        public void ShouldRequireMinimumFields()
        {
            // Arrange
            var command = new CreateProjectCommand();

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Key")
                .WhichValue.Contains("Key is required.");
        }

        [Theory]
        [InlineData("","Key", "Key is required.")]
        [InlineData("XY", "Key", "Key should not less than 3 characters.")]
        [InlineData("TESTKEY", "Key", "Key must not exceed 4 characters.")]
        [InlineData("TEST KEY", "Key", "Key should be one word long only.")]
        [InlineData("test", "Key", "Key should be all in Upper Case and should only include alphabetic characters.")]
        [InlineData("TEST3", "Key", "Key should be all in Upper Case and should only include alphabetic characters.")]

        public void ShouldRequireValidProjectKey(string key, string errorKey, string errorValue)
        {
            // Arrange
            var command = new CreateProjectCommand
            {
                Key = key
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey(errorKey)
                .WhichValue.Contains(errorValue);
        }

        [Theory]
        [InlineData("", "Name", "Name is required.")]
        [InlineData(null, "Name", "Name is required.")]
        [InlineData("This is test project name which is exceeding 20 characters.", "Name", "Name must not exceed 20 characters.")]
        public void ShouldRequireValidProjectName(string name, string errorKey, string errorValue)
        {
            // Arrange
            var command = new CreateProjectCommand
            {
                Key = "TKWZ",
                Name = name
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey(errorKey)
                .WhichValue.Contains(errorValue);
        }

        [Fact]
        public async Task ShouldNotAllowToCreateSameKeyProject()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var command = new CreateProjectCommand
            {
                Key = "TRWZ",
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(command);
            var duplicateProject = FluentActions.Invoking(() => _fixture.SendAsync(command));
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(command.Name);
            item.Key.Should().Be(command.Key);
            item.CreatedBy.Should().Be(userId);
            item.Created.Should().BeCloseTo(DateTime.UtcNow, 10000);
            item.LastModifiedBy.Should().BeNull();
            item.LastModified.Should().BeNull();

            duplicateProject.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Key")
                .WhichValue.Contains("Key already exists.");
        }

        [Fact]
        public async Task ShouldBeAbleToCreateNewProject()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var command = new CreateProjectCommand
            {
                Key = "TRWZ",
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(command);
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(command.Name);
            item.Key.Should().Be(command.Key);
            item.CreatedBy.Should().Be(userId);
            item.Created.Should().BeCloseTo(DateTime.UtcNow, 10000);
            item.LastModifiedBy.Should().BeNull();
            item.LastModified.Should().BeNull();
        }
    }
}
