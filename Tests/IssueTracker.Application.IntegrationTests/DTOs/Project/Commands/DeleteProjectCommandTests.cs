﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.DTOs.Project.Commands.DeleteProject;
using IssueTracker.Application.Exceptions;
using MediatR;
using Xunit;

namespace IssueTracker.Application.IntegrationTests.DTOs.Project.Commands
{
    public class DeleteProjectCommandTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;

        public DeleteProjectCommandTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void ShouldRequireMinimumFields()
        {
            // Arrange
            var command = new DeleteProjectCommand();

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("Id is required.");
        }

        [Fact]
        public async Task ShouldThrowErrorWhenTryingToDeleteAProjectWhichUserDoNotOwn()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "TRKZ");
            var newUserId = await _fixture.RunAsUserAsync("newuser@local", "Testing1234!");
            var command = new DeleteProjectCommand { Id = projectId };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("User is not allowed to delete the project.");
        }

        [Fact]
        public async Task ShouldBeAbleToDeleteTheProject()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "DRWZ");
            var command = new DeleteProjectCommand { Id = projectId };

            // Act
            var actual = await _fixture.SendAsync(command);
            var fetchProject = await _fixture.FindAsync<Domain.Entities.Project>(projectId);

            // Assert
            actual.Should().NotBeNull();
            actual.Data.Should().Be(Unit.Value);
            actual.Succeeded.Should().Be(true);
            actual.Message.Should().Be("Project deleted successfully.");
            fetchProject.Should().BeNull();
        }

        private async Task<int> CreateNewTestProject(string userId, string key)
        {
            var createProjectCommand = new CreateProjectCommand
            {
                Key = key,
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(createProjectCommand);
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(createProjectCommand.Name);
            item.Key.Should().Be(createProjectCommand.Key);

            return project.Data;
        }
    }
}

