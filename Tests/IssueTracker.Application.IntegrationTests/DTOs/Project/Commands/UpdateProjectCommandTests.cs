﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.DTOs.Project.Commands.UpdateProject;
using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.IntegrationTests.DTOs.Project.Commands
{
    public class UpdateProjectCommandTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;
        public UpdateProjectCommandTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void ShouldRequireMinimumFields()
        {
            // Arrange
            var command = new UpdateProjectCommand();

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("Id is required.");
        }

        [Fact]
        public void ShouldNotAllowToUpdateNonExistingProject()
        {
            // Arrange
            var command = new UpdateProjectCommand
            {
                Id = 0
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("Id does not exists.");
        }

        [Fact]
        public async Task ShouldNotAllowToUpdateProjectDetailsIfUserIsNotOwnerOfTheProject()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "UTST");
            var newUserId = await _fixture.RunAsUserAsync("newuser1@local", "Testing1234!");

            var command = new UpdateProjectCommand
            {
                Id = projectId,
                Description = "Test Description",
                Name = "Test Name"
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("User is not allowed to delete the project.");
        }

        [Fact]
        public async Task ShouldBeAbleToUpdateTheProjectDetails()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            //var projectId = await CreateNewTestProject(userId, "ATST");

            var createCommand = new CreateProjectCommand
            {
                Key = "ATST",
                Name = "Test Project"
            };

            var updateCommand = new UpdateProjectCommand
            {
                Name = "New Test Project",
                Description = "New Test Description"
            };

            // Act
            var project = await _fixture.SendAsync(createCommand);
            var createdProject = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);
            updateCommand.Id = project.Data;
            await _fixture.SendAsync(updateCommand);
            var updatedProject = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            createdProject.Should().NotBeNull();
            createdProject.Name.Should().Be(createCommand.Name);
            createdProject.Key.Should().Be(createCommand.Key);
            createdProject.CreatedBy.Should().Be(userId);
            createdProject.Created.Should().BeCloseTo(DateTime.UtcNow, 10000);
            createdProject.LastModifiedBy.Should().BeNull();
            createdProject.LastModified.Should().BeNull();

            updatedProject.Should().NotBeNull();
            updatedProject.Name.Should().Be(updateCommand.Name);
            updatedProject.Description.Should().Be(updateCommand.Description);
            updatedProject.CreatedBy.Should().Be(userId);
            updatedProject.Created.Should().BeCloseTo(DateTime.UtcNow, 10000);
            updatedProject.LastModifiedBy.Should().Be(userId);
            updatedProject.LastModified.Should().BeCloseTo(DateTime.UtcNow, 10000);
        }

        private async Task<int> CreateNewTestProject(string userId, string key)
        {
            var createProjectCommand = new CreateProjectCommand
            {
                Key = key,
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(createProjectCommand);
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(createProjectCommand.Name);
            item.Key.Should().Be(createProjectCommand.Key);

            return project.Data;
        }
    }
}
