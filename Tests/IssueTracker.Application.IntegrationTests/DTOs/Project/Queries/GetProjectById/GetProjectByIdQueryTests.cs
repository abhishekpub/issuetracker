﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.DTOs.Project.Queries.GetProjectById;
using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.IntegrationTests.DTOs.Project.Queries.GetProjectById
{
    public class GetProjectByIdQueryTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;

        public GetProjectByIdQueryTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void ShouldRequireMinimumFields()
        {
            // Arrange
            // Act
            var command = new GetProjectByIdQuery();

            // Assert
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("Id is required.");
        }

        [Fact]
        public void ShouldThrowErrorWhenInvalidProjectId()
        {
            // Arrange
            var command = new GetProjectByIdQuery()
            {
                Id = 0
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Id")
                .WhichValue.Contains("Id does not exists.");
        }

        [Fact]
        public async Task ShouldBeAbleToGetByProjectId()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var createProjectCommand = new CreateProjectCommand
            {
                Key = "GRWZ",
                Name = "Test Project"
            };
            var command = new GetProjectByIdQuery();

            // Act
            var project = await _fixture.SendAsync(createProjectCommand);
            command.Id = project.Data;
            var projectData = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);
            var actual = await _fixture.SendAsync(command);

            // Assert
            actual.Should().NotBeNull();
            actual.Data.Should().BeEquivalentTo(projectData);
            actual.Succeeded.Should().Be(true);
        }
    }
}
