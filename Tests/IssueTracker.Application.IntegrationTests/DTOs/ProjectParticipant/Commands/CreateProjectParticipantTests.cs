﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant;
using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.IntegrationTests.DTOs.ProjectParticipant.Commands
{
    public class CreateProjectParticipantTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;

        public CreateProjectParticipantTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
            Task.Run(async () => await _fixture.ResetState());
        }

        [Theory]
        [InlineData(0, "ProjectId", "ProjectId does not exists.")]
        [InlineData(null, "ProjectId", "ProjectId is required.")]
        public void ShouldRequireMinimumFields(int? projectId, string errorKey, string errorMessage)
        {
            // Arrange
            var command = new CreateProjectParticipantCommand();
            if (projectId != null)
            {
                command.ProjectId = (int) projectId;
            }
            
            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey(errorKey)
                .WhichValue.Contains(errorMessage);
        }

        [Fact]
        public async Task ShouldRequireValidProjectId()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "CPRJ");
            var newUserId = await _fixture.RunAsUserAsync("user1@local","Test123!");

            var command = new CreateProjectParticipantCommand
            {
                ProjectId = projectId
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("ProjectId")
                .WhichValue.Contains("You can not add participants since you do not own requested project.");
        }

        [Theory]
        [InlineData(null, "Users", "Users is required.")]
        [InlineData(new string[] {}, "Users", "Atleast one Users required to add project participant.")]
        [InlineData(new string[] { "user1@gmail.com", "user2@gmail.com" }, "Users", "Users does not exists.")]
        public async Task ShouldRequireValidUsersToAddIntoProjectParticipant(string[] userNames, string errorKey, string errorMessage)
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "CPRJ");

            var command = new CreateProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = userNames?.ToList()
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey(errorKey)
                .WhichValue.Contains(errorMessage);
        }

        [Fact]
        public async Task ShouldBeAbleToAddIntoProjectParticipant()
        {
            // Arrange
            var user1EmailId = "user1@gmail.com";
            var user1 = await _fixture.RunAsUserAsync(user1EmailId, "Test123!");
            
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "CPRJ");
            var command = new CreateProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = new List<string>(){ user1EmailId }
            };

            // Act
            var actual = await _fixture.SendAsync(command);

            // Assert
            actual.Should().NotBeNull();
            actual.Data.ProjectId.Should().Be(projectId);
            actual.Data.UsersAddedSuccessfully.Should().HaveCount(command.Users.Count);
            actual.Data.UsersAddedSuccessfully.Should().Contain(user1EmailId);
            actual.Data.UsersFailedToAdd.Should().HaveCount(0);
        }

        private async Task<int> CreateNewTestProject(string userId, string key)
        {
            var createProjectCommand = new CreateProjectCommand
            {
                Key = key,
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(createProjectCommand);
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(createProjectCommand.Name);
            item.Key.Should().Be(createProjectCommand.Key);

            return project.Data;
        }
    }
}
