﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant;
using IssueTracker.Application.DTOs.ProjectParticipant.Commands.DeleteProjectParticipant;
using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.IntegrationTests.DTOs.ProjectParticipant.Commands
{
    public class DeleteProjectParticipantTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;

        public DeleteProjectParticipantTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
            Task.Run(async () => await _fixture.ResetState());
        }

        [Theory]
        [InlineData(0, "ProjectId", "ProjectId does not exists.")]
        [InlineData(null, "ProjectId", "ProjectId does not exists.")]
        public void ShouldRequireMinimumFields(int? projectId, string errorKey, string errorMessage)
        {
            // Arrange
            var command = new DeleteProjectParticipantCommand();
            if (projectId != null)
            {
                command.ProjectId = (int)projectId;
            }

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey(errorKey)
                .WhichValue.Contains(errorMessage);
        }

        [Fact]
        public async Task ShouldNotAbleToDeleteProjectParticipantByOtherUsers()
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "PRT");
            var deleteProjectParticipantCommand = new DeleteProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = new List<string>()
            };

            // Act
            var user1 = await _fixture.RunAsUserAsync("firstuser@local", "Test123!");
            deleteProjectParticipantCommand.Users.Add(user1);
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(deleteProjectParticipantCommand));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("ProjectId")
                .WhichValue.Contains("You can not delete participants from projects which you do not own.");
        }

        [Theory]
        [InlineData(null, "Users", "Users is required.")]
        [InlineData(new string[] { }, "Users", "Atleast one Users required to add project participant.")]
        [InlineData(new string[] { "user1@gmail.com", "user2@gmail.com" }, "Users", "Users does not exists.")]
        public async Task ShouldRequireValidUsersToToDeleteProjectParticipant(string[] userNames, string errorKey, string errorMessage)
        {
            // Arrange
            var userId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(userId, "CPRJ");

            var command = new CreateProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = userNames?.ToList()
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey(errorKey)
                .WhichValue.Contains(errorMessage);
        }

        [Fact]
        public async Task ShouldNotAllowToDeleteProjectOwnerFromParticipants()
        {
            // Arrange
            var projectOwnerEmail = "test@local";
            var userId = await _fixture.RunAsUserAsync(projectOwnerEmail,"Test123!");
            var projectId = await CreateNewTestProject(userId, "UPRJ");

            var command = new DeleteProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = new List<string> { projectOwnerEmail }
            };

            // Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            // Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Errors.Should().ContainKey("Users")
                .WhichValue.Contains("Owner can only be delete if associated project will get deleted.");
        }

        [Fact]
        public async Task ShouldRequireProjectOwnerToDeleteProjectParticipant()
        {
            // Arrange
            const string anotherUserEmail = "firstuser@local";
            var anotherUserId = await _fixture.RunAsUserAsync(anotherUserEmail, "Test123!");
            var ownerUserId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(ownerUserId, "PRT");
            var createProjectParticipantCommand = new CreateProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = new List<string>()
            };

            var deleteProjectParticipantCommand = new DeleteProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = new List<string>()
            };

            // Act
            createProjectParticipantCommand.Users.Add(anotherUserEmail);
            var createParticipantResult = await _fixture.SendAsync(createProjectParticipantCommand);

            // Perform delete participant
            deleteProjectParticipantCommand.Users.Add(anotherUserEmail);
            var deleteProjectParticipantResult = await _fixture.SendAsync(deleteProjectParticipantCommand);

            // Assert
            // Assert to check if project participant added successfully.
            createParticipantResult.Should().NotBeNull();
            createParticipantResult.Data.ProjectId.Should().Be(projectId);
            createParticipantResult.Data.UsersAddedSuccessfully.Should().HaveCount(createProjectParticipantCommand.Users.Count);
            createParticipantResult.Data.UsersAddedSuccessfully.Should().Contain(anotherUserEmail);
            createParticipantResult.Data.UsersFailedToAdd.Should().HaveCount(0);

            // Assert to check if project participant deleted successfully.
            deleteProjectParticipantResult.Should().NotBeNull();
            deleteProjectParticipantResult.Succeeded.Should().BeTrue();
            deleteProjectParticipantResult.Message.Should()
                .Be($"Participants successfully removed from the project id {projectId}.");

        }

        private async Task<int> CreateNewTestProject(string userId, string key)
        {
            var createProjectCommand = new CreateProjectCommand
            {
                Key = key,
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(createProjectCommand);
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(createProjectCommand.Name);
            item.Key.Should().Be(createProjectCommand.Key);

            return project.Data;
        }
    }
}
