﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using FluentAssertions.Common;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.DTOs.Project.Queries.GetProjectById;
using IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant;
using IssueTracker.Application.DTOs.ProjectParticipant.Queries.GetAllProjectParticipants;
using IssueTracker.Application.Exceptions;
using MediatR;
using Microsoft.Data.SqlClient.Server;
using Xunit;


namespace IssueTracker.Application.IntegrationTests.DTOs.ProjectParticipant.Queries
{
    public class GetAllProjectParticipantByIdTests : IClassFixture<InMemoryDbContextFixture>
    {
        private readonly InMemoryDbContextFixture _fixture;

        public GetAllProjectParticipantByIdTests(InMemoryDbContextFixture fixture)
        {
            _fixture = fixture;
            Task.Run(async () => await _fixture.ResetState());
        }

        [Fact]
        public void ShouldThrowErrorWhenInvalidParameters()
        {
            //Arrange
            var command = new GetProjectParticipantsByProjectIdQuery(){ 
            PageNumber = -1,
            PageSize = -1,
            ProjectId = 1
            };

            //Act
            var actual = FluentActions.Invoking(() => _fixture.SendAsync(command));

            //Assert
            actual.Should()
                .Throw<ValidationException>()
                .Which.Message.Contains("One or more validation failures have occurred.");
        }

        [Fact]
        public async Task ShouldBeAbleToGetProjectParticipantsByProjectId()
        {
            //Arrange
            const string anotherUserEmail = "firstuser@local";
            var anotherUserId = await _fixture.RunAsUserAsync(anotherUserEmail, "Test123!");
            var ownerUserId = await _fixture.RunAsDefaultUserAsync();
            var projectId = await CreateNewTestProject(ownerUserId, "PRT");
            var createProjectParticipantCommand = new CreateProjectParticipantCommand
            {
                ProjectId = projectId,
                Users = new List<string>()
            };
            var command = new GetProjectParticipantsByProjectIdQuery()
            {
                PageNumber = 1,
                PageSize = 10,
                ProjectId = 1
            };

            // Act
            createProjectParticipantCommand.Users.Add(anotherUserEmail);
            var createParticipantResult = await _fixture.SendAsync(createProjectParticipantCommand);

            //Act get All participants
            var actual = await _fixture.SendAsync(command);

            // Assert
            // Assert to check if project participant added successfully.
            createParticipantResult.Should().NotBeNull();
            createParticipantResult.Data.ProjectId.Should().Be(projectId);
            createParticipantResult.Data.UsersAddedSuccessfully.Should().HaveCount(createProjectParticipantCommand.Users.Count);
            createParticipantResult.Data.UsersAddedSuccessfully.Should().Contain(anotherUserEmail);
            createParticipantResult.Data.UsersFailedToAdd.Should().HaveCount(0); 

            //Assert
            actual.Should().NotBeNull();
            actual.Data.Should().HaveCount(2);
            actual.Succeeded.Should().Be(true);
        }

        private async Task<int> CreateNewTestProject(string userId, string key)
        {
            var createProjectCommand = new CreateProjectCommand
            {
                Key = key,
                Name = "Test Project"
            };

            // Act
            var project = await _fixture.SendAsync(createProjectCommand);
            var item = await _fixture.FindAsync<Domain.Entities.Project>(project.Data);

            // Assert
            item.Should().NotBeNull();
            item.Name.Should().Be(createProjectCommand.Name);
            item.Key.Should().Be(createProjectCommand.Key);

            return project.Data;
        }
    }
}
