﻿using System;
using AutoMapper;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.Mappings;
using IssueTracker.Application.Models;
using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;
using Xunit;

namespace IssueTracker.Application.UnitTests.Common.Mappings
{
    public class MappingTests
    {
        private readonly IConfigurationProvider _configuration;
        private readonly IMapper _mapper;

        public MappingTests()
        {
            _configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<GeneralProfile>();
            });

            _mapper = _configuration.CreateMapper();
        }

        [Theory]
        [InlineData(typeof(CreateProjectCommand), typeof(Project))]
        [InlineData(typeof(ApplicationUser), typeof(GetAllUsersQueryViewModel))]
        public void ShouldSupportMappingFromSourceToDestination(Type source, Type destination)
        {
            var instance = Activator.CreateInstance(source);

            _mapper.Map(instance, source, destination);
        }
    }
}
