using System.Collections.Generic;
using FluentValidation.Results;
using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.UnitTests.Common.Exceptions
{
    public class ValidationExceptionTests
    {
        [Fact]
        public void DefaultConstructorCreatesAnEmptyErrorDictionary()
        {
            // Arrange
            // Act
            var actual = new ValidationException().Errors;

            // Assert
            Assert.Equal(actual, new Dictionary<string, string[]>());
        }

        [Fact]
        public void SingleValidationFailureCreatesASingleElementErrorDictionary()
        {
            // Arrange
            var failures = new List<ValidationFailure>
            {
                new ValidationFailure("Age", "must be over 18"),
            };

            // Act
            var actual = new ValidationException(failures).Errors;

            // Assert
            Assert.Equal(actual.Keys, new string[] { "Age" });
            Assert.Equal(actual["Age"], new string[] { "must be over 18" });
        }

        [Fact]
        public void MulitpleValidationFailureForMultiplePropertiesCreatesAMultipleElementErrorDictionaryEachWithMultipleValues()
        {
            // Arrange
            var failures = new List<ValidationFailure>
            {
                new ValidationFailure("Age", "must be 18 or older"),
                new ValidationFailure("Age", "must be 25 or younger"),
                new ValidationFailure("Password", "must contain at least 8 characters"),
                new ValidationFailure("Password", "must contain a digit"),
                new ValidationFailure("Password", "must contain upper case letter"),
                new ValidationFailure("Password", "must contain lower case letter"),
            };

            // Act
            var actual = new ValidationException(failures).Errors;

            // Assert
            Assert.Equal(actual.Keys, new[] { "Age", "Password" });
            Assert.Equal(actual["Age"], new[]
            {
                "must be 18 or older",
                "must be 25 or younger",
            });
            Assert.Equal(actual["Password"], new[]
            {
                "must contain at least 8 characters",
                "must contain a digit",
                "must contain upper case letter",
                "must contain lower case letter",
            });
        }
    }
}
