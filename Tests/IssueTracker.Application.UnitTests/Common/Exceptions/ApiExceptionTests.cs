using IssueTracker.Application.Exceptions;
using Xunit;

namespace IssueTracker.Application.UnitTests.Common.Exceptions
{
    public class ApiExceptionTests
    {
        [Fact]
        public void DefaultConstructorCreatesDefaultExceptionMessage()
        {
            // Arrange
            // Act
            var actual = new ApiException();

            // Assert
            Assert.Equal(actual.Message, $"Exception of type '{typeof(ApiException)}' was thrown.");
        }

        [Fact]
        public void ConstructorWithMessageCreatesCustomExceptionMessage()
        {
            // Arrange
            const string message = "Test Exception";

            // Act
            var actual = new ApiException(message);

            // Assert
            Assert.Equal(actual.Message, message);
        }

        [Fact]
        public void ConstructorWithMessageAndArgumentsCreatesCustomExceptionMessage()
        {
            // Arrange
            const string message = "Test Exception";

            // Act
            var actual = new ApiException(message, "param1");

            // Assert
            Assert.Equal(actual.Message, message);
        }
    }
}
