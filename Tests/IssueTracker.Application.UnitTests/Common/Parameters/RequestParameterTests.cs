﻿using IssueTracker.Application.Parameters;
using Xunit;

namespace IssueTracker.Application.UnitTests.Common.Parameters
{
    public class RequestParameterTests
    {
        [Fact]
        public void DefaultConstructorShouldCreateDefaultPropertyValues()
        {
            // Arrange
            // Act
            var requestParameter = new RequestParameter();

            // Assert
            Assert.Equal(1, requestParameter.PageNumber);
            Assert.Equal(10,requestParameter.PageSize);
        }

        [Theory]
        [InlineData(10, 20)]
        [InlineData(0, 0)]
        [InlineData(-1, 100)]
        [InlineData(100, -1)]
        public void ConstructorShouldCreateCustomPropertyValues(int pageNumber, int pageSize)
        {
            // Arrange
            // Act
            var requestParameter = new RequestParameter(pageNumber, pageSize);

            // Assert
            Assert.Equal(pageNumber < 1 ? 1 : pageNumber, requestParameter.PageNumber);
            Assert.Equal(pageSize > 10 ? 10 : pageSize, requestParameter.PageSize);
        }
    }
}
