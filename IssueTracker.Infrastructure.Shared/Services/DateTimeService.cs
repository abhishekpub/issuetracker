﻿using IssueTracker.Application.Interfaces;
using System;

namespace IssueTracker.Infrastructure.Shared.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime NowUtc => DateTime.UtcNow;
    }
}
