﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.Wrappers
{
    public abstract class BaseValidator<T> : AbstractValidator<T>
    {
        // TODO: Wrong design need to refactor this
        protected abstract IApplicationDbContext Context { get; set; }
        protected abstract IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected virtual UserManager<ApplicationUser> UserManager { get; set; }

        protected async Task<bool> ProjectShouldExists(int projectId, CancellationToken cancellationToken)
        {
            var project = await Context.Projects.FindAsync(projectId);

            return project != null;
        }

        protected async Task<bool> ShouldOwnerOfProject(int projectId, CancellationToken cancellationToken)
        {
            var project = await Context.Projects.FindAsync(projectId);

            if (project != null)
            {
                return project.CreatedBy == AuthenticatedUser.UserId;
            }

            return false;
        }

        protected bool UserShouldExist(string userEmail, CancellationToken cancellationToken)
        {
            var user = UserManager.Users.SingleOrDefault(x => x.Email.ToLower() == userEmail.ToLower());

            return user != null;
        }

        protected async Task<bool> IssueShouldExist(int issueId, CancellationToken cancellationToken)
        {
            var dbIssueType = await Context.Issues.FirstOrDefaultAsync(
                x => x.Id == issueId, cancellationToken: cancellationToken);

            return dbIssueType != null;
        }

        protected async Task<bool> UserIsInProjectParticipant(int projectId, CancellationToken cancellationToken)
        {
            var project = await Context.Projects.Include(x => x.ProjectParticipants)
                .FirstOrDefaultAsync(x => x.Id == projectId, cancellationToken: cancellationToken);

            if (project != null)
            {
                var requestedUser = UserManager.Users.SingleOrDefault(x => x.Id == AuthenticatedUser.UserId);

                if (requestedUser != null)
                {
                    if (project.ProjectParticipants.Any(x => x.UserId == requestedUser.Id))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
