﻿namespace IssueTracker.Application.Enums
{
    public enum IssueType
    {
        Story,
        Bug,
        Task,
    }
}