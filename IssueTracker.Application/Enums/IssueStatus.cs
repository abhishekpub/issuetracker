﻿using System.ComponentModel;

namespace IssueTracker.Application.Enums
{
    public enum IssueStatus
    {
        [Description("Todo")]
        Todo = 1,

        [Description("InProgress")]
        InProgress = 2,

        [Description("Done")]
        Done = 3
    }
}