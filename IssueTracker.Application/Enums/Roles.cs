﻿namespace IssueTracker.Application.Enums
{
    public enum Roles
    {
        SuperAdmin,
        Admin,
        User
    }
}
