﻿using AutoMapper;
using IssueTracker.Application.DTOs.Account.Queries.GetAllUsers;
using IssueTracker.Application.DTOs.Issues.Command.CreateIssue;
using IssueTracker.Application.DTOs.Issues.Command.UpdateIssue;
using IssueTracker.Application.DTOs.Project.Commands.CreateProject;
using IssueTracker.Application.Models;
using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;

namespace IssueTracker.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<CreateProjectCommand, Project>();
            CreateMap<ApplicationUser, GetAllUsersQueryViewModel>().ReverseMap();
            CreateMap<CreateIssueCommand, Issue>().ReverseMap()
                .ForMember(dest=>dest.Assignee, src=>src.Ignore());
            CreateMap<UpdateIssueCommand, Issue>()
                .ForMember(dest => dest.AssignedTo, src => src.Ignore())
                .ForMember(dest => dest.ProjectId, src => src.Ignore());

            CreateMap<Issue, GetAllIssuesQueryViewModel>();
        }
    }
}
