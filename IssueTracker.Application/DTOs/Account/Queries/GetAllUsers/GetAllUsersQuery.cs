﻿using System.Collections.Generic;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Account.Queries.GetAllUsers
{
    public class GetAllUsersQuery : IRequest<PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
