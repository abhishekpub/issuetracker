﻿using System.Collections.Generic;
using IssueTracker.Application.Models;

namespace IssueTracker.Application.DTOs.Account.Queries.GetAllUsers
{
    public class GetAllIssuesQueryViewModel
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Title { get; set; }

        public string Assignee { get; set; }

        public string Parent { get; set; }

        public string Type { get; set; }

        public IEnumerable<CustomFieldInfo> CustomFieldInfo { get; set; }
    }
}
