﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Account.Queries.GetAllUsers
{
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        public GetAllUsersQueryHandler(UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>> Handle(GetAllUsersQuery request,
            CancellationToken cancellationToken)
        {
            var applicationUsers = await _userManager.Users.Skip((request.PageNumber - 1) * request.PageSize)
                .Take(request.PageSize)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);

            var allUsers = _mapper.Map<IEnumerable<GetAllUsersQueryViewModel>>(applicationUsers);
            
            return new PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>(allUsers, request.PageNumber,
                request.PageSize);
        }

        private static GetAllUsersQueryViewModel GetAllUsersQueryViewModel(ApplicationUser x)
        {
            return new GetAllUsersQueryViewModel
            {
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName,
                UserName = x.UserName,
            };
        }
    }
}
