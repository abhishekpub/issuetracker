﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Exceptions;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant
{
    public class CreateProjectParticipantCommandHandler : IRequestHandler<CreateProjectParticipantCommand, Response<CreateProjectParticipantResponse>>
    {
        private readonly IApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthenticatedUserService _authenticatedUser;

        public CreateProjectParticipantCommandHandler(IApplicationDbContext context,
            UserManager<ApplicationUser> userManager, IAuthenticatedUserService authenticatedUser)
        {
            _context = context;
            _userManager = userManager;
            _authenticatedUser = authenticatedUser;
        }

        public async Task<Response<CreateProjectParticipantResponse>> Handle(CreateProjectParticipantCommand request,
            CancellationToken cancellationToken)
        {
            // Report error if project does not exists
            var existingProject = _context.Projects.Include(x => x.ProjectParticipants)
                .FirstOrDefault(x => x.Id == request.ProjectId);
            if (existingProject == null)
            {
                throw new ApiException("Project not found");
            }

            var usersFailedToAdd = new List<string>();
            var usersSuccessfullyToAdded = new List<string>();
            foreach (var user in request.Users)
            {
                var existingUser = _userManager.Users.SingleOrDefault(x => x.Email.ToLower() == user.ToLower());
                if (existingUser != null)
                {
                    var projectParticipant = new Domain.Entities.ProjectParticipant
                    {
                        UserId = existingUser.Id,
                        ProjectId = request.ProjectId
                    };

                    // Check if the user with project Id is already exists in participant list (can be owner)
                    var existingProjectParticipant =
                        existingProject?.ProjectParticipants.FirstOrDefault(x => x.UserId == existingUser.Id);
                    if (existingProjectParticipant == null)
                    {
                        existingProject?.ProjectParticipants.Add(projectParticipant);
                        usersSuccessfullyToAdded.Add(existingUser.Email);
                    }
                }
                else
                {
                    usersFailedToAdd.Add(user);
                }
            }
            
            // Prepare response
            var response = new CreateProjectParticipantResponse
            {
                ProjectId = request.ProjectId,
                UsersAddedSuccessfully = usersSuccessfullyToAdded,
                UsersFailedToAdd = usersFailedToAdd
            };

            _context.Projects.Update(existingProject);
            await _context.SaveChangesAsync(cancellationToken);
            return new Response<CreateProjectParticipantResponse>(response);
        }
    }
}
