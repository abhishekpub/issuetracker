﻿using System.Collections.Generic;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant
{
    public class CreateProjectParticipantCommand : IRequest<Response<CreateProjectParticipantResponse>>
    {
        public int ProjectId { get; set; }

        public List<string> Users { get; set; }
    }
}
