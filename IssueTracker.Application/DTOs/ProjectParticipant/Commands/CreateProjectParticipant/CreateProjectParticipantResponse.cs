﻿using System.Collections.Generic;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant
{
    public class CreateProjectParticipantResponse
    {
        public int ProjectId { get; set; }

        public List<string> UsersAddedSuccessfully { get; set; }

        public List<string> UsersFailedToAdd { get; set; }
    }
}
