﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant
{
    public class CreateProjectParticipantCommandValidator : BaseValidator<CreateProjectParticipantCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        private UserManager<ApplicationUser> UserManager { get; set; }

        public CreateProjectParticipantCommandValidator(IApplicationDbContext context, UserManager<ApplicationUser> userManager, IAuthenticatedUserService authenticatedUser)
        {
            Context = context;
            UserManager = userManager;
            AuthenticatedUser = authenticatedUser;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.ProjectId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.")
                .MustAsync(ShouldOwnerOfProject).WithMessage("You can not add participants since you do not own requested project.");

            RuleFor(v => v.Users)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .Must(x => x.Count > 0).WithMessage("Atleast one {PropertyName} required to add project participant.")
                .MustAsync(UsersNotExists).WithMessage("{PropertyName} does not exists.");
        }

        private async Task<bool> UsersNotExists(List<string> users, CancellationToken cancellationToken)
        {
            var userIds = users.Select(x => x.ToLower()).ToList();
            var usersExists = await UserManager.Users.AnyAsync(
                x => userIds.Any(y=> y == x.Email.ToLower()), cancellationToken);

            return usersExists;
        }
    }
}
