﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.DeleteProjectParticipant
{
    public class DeleteProjectParticipantCommandHandler : IRequestHandler<DeleteProjectParticipantCommand, Response<Unit>>
    {
        private readonly IApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public DeleteProjectParticipantCommandHandler(IApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<Response<Unit>> Handle(DeleteProjectParticipantCommand request, CancellationToken cancellationToken)
        {
            var project = _context.Projects.Include(x => x.ProjectParticipants)
                .First(x => x.Id == request.ProjectId);

            foreach (var user in request.Users)
            {
                var applicationUser = _userManager.Users.SingleOrDefault(x => x.Email.ToLower() == user.ToLower());
                if (applicationUser != null)
                {
                    var participant =
                        project.ProjectParticipants.FirstOrDefault(x => x.UserId == applicationUser.Id);

                    if (participant != null)
                    {
                        _context.ProjectParticipants.Remove(participant);
                    }
                }
            }

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<Unit>(Unit.Value, $"Participants successfully removed from the project id {request.ProjectId}.");
        }
    }
}
