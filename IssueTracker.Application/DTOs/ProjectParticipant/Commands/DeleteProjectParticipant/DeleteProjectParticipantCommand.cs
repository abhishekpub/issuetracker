﻿using System.Collections.Generic;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.DeleteProjectParticipant
{
    public class DeleteProjectParticipantCommand : IRequest<Response<Unit>>
    {
        public int ProjectId { get; set; }

        public List<string> Users { get; set; }
    }
}
