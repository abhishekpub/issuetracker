﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Commands.DeleteProjectParticipant
{
    public class DeleteProjectParticipantCommandValidator : BaseValidator<DeleteProjectParticipantCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected UserManager<ApplicationUser> UserManager { get; set; }

        public DeleteProjectParticipantCommandValidator(IApplicationDbContext context, IAuthenticatedUserService authenticatedUser, UserManager<ApplicationUser> userManager)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            UserManager = userManager;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.ProjectId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.")
                .MustAsync(IsProjectOwner).WithMessage("You can not delete participants from projects which you do not own.");

            RuleFor(v => v.Users)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .Must(x => x.Count > 0).WithMessage("Atleast one {PropertyName} required to add project participant.")
                .MustAsync(UsersNotExists).WithMessage("Users does not exists.");

            RuleFor(m => m)
                .Must(x => UsersShouldNotContainsProjectOwner(x.ProjectId, x.Users))
                .WithMessage("Owner can only be delete if associated project will get deleted.")
                .WithName("Users");

            //TODO: add validation on Users to validate email address.
        }

        private bool UsersShouldNotContainsProjectOwner(int projectId, List<string> users)
        {
            var project = Context.Projects.Find(projectId);
            if (project != null)
            {
                var projectOwner = UserManager.Users.SingleOrDefault(x=>x.Id == project.CreatedBy);

                if (projectOwner != null)
                {
                    if (users.Select(x => x.ToLower()).ToList().Contains(projectOwner.Email.ToLower()))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private async Task<bool> IsProjectOwner(int projectId, CancellationToken cancellationToken)
        {
            var project = await Context.Projects.FindAsync(projectId);
            return project != null && project.CreatedBy == AuthenticatedUser.UserId;
        }

        private async Task<bool> UsersNotExists(List<string> users, CancellationToken cancellationToken)
        {
            var userIds = users.Select(x => x.ToLower()).ToList();

            var usersExists = await UserManager.Users.AnyAsync(
                x => userIds.Any(y => y == x.Email.ToLower()), cancellationToken);

            return usersExists;
        }
    }
}
