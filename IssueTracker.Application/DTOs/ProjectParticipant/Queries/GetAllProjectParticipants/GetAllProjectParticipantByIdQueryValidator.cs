﻿using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Queries.GetAllProjectParticipants
{
    public class GetAllProjectParticipantByIdQueryValidator : BaseValidator<GetProjectParticipantsByProjectIdQuery>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected sealed override UserManager<ApplicationUser> UserManager { get; set; }

        public GetAllProjectParticipantByIdQueryValidator(IAuthenticatedUserService authenticatedUser, IApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            AuthenticatedUser = authenticatedUser;
            UserManager = userManager;
            Context = context;

            RuleFor(v => v.ProjectId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.")
                .MustAsync(UserIsInProjectParticipant).WithMessage("User does not belong to requested Project.");
        }
    }
}
