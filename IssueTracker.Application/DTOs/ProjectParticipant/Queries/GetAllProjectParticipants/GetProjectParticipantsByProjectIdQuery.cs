﻿using System.Collections.Generic;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Queries.GetAllProjectParticipants
{
    public class GetProjectParticipantsByProjectIdQuery : IRequest<PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int ProjectId { get; set; }
    }
}
