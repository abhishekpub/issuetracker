﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.Exceptions;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.ProjectParticipant.Queries.GetAllProjectParticipants
{
    public class GetAllProjectParticipantByIdQueryHandler : IRequestHandler<GetProjectParticipantsByProjectIdQuery,
        PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>>
    {
        private readonly IApplicationDbContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public GetAllProjectParticipantByIdQueryHandler(IApplicationDbContext applicationDbContext,
            UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
            _mapper = mapper;
        }
        public async Task<PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>> Handle(
            GetProjectParticipantsByProjectIdQuery request, CancellationToken cancellationToken)
        {
            var project = await _applicationDbContext.Projects
                .Include(x => x.Issues)
                .Include(x=>x.ProjectParticipants)
                .FirstOrDefaultAsync(x => x.Id == request.ProjectId, cancellationToken: cancellationToken);

            if (project == null)
            {
                throw new ApiException($"Project Not Found.");
            }

            var participantsUserIds = project.ProjectParticipants.Skip((request.PageNumber - 1) * request.PageSize)
                .Take(request.PageSize).Select(x => x.UserId);
            var applicationUsers = _userManager.Users.Where(x => participantsUserIds.Contains(x.Id));
            var allUsers = _mapper.Map<IEnumerable<GetAllUsersQueryViewModel>>(applicationUsers);

            return new PagedResponse<IEnumerable<GetAllUsersQueryViewModel>>(allUsers, request.PageNumber,
                request.PageSize);
        }
    }
}
