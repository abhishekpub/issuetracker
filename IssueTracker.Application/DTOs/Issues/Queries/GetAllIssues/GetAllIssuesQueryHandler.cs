﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.DTOs.Account.Queries.GetAllUsers;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Issues.Queries.GetAllIssues
{
    public class GetAllIssuesQueryHandler : IRequestHandler<GetAllIssuesQuery, PagedResponse<IEnumerable<GetAllIssuesQueryViewModel>>>
    {
        private readonly IApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public GetAllIssuesQueryHandler(IApplicationDbContext applicationDbContext, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<PagedResponse<IEnumerable<GetAllIssuesQueryViewModel>>> Handle(GetAllIssuesQuery request, CancellationToken cancellationToken)
        {
            // Apply filters
            var issues = new List<GetAllIssuesQueryViewModel>();

            var totalIssues = _applicationDbContext.Issues
                .Include(x => x.IssueType)
                .Include(x => x.Status)
                .Include(x => x.CustomFieldValues)
                .ThenInclude(x=>x.CustomField)
                .Include(x => x.Project).AsNoTracking();

            if (!string.IsNullOrWhiteSpace(request.Assignee))
            {
                var userId = _userManager.Users.FirstOrDefault(x => x.Email.ToLower() == request.Assignee.ToLower())?.Id;
                totalIssues = totalIssues.Where(x => x.AssignedTo == userId);
            }

            if (request.ProjectId != null)
            {
                totalIssues = totalIssues.Where(x => x.ProjectId == request.ProjectId);
            }

            // Apply paging
            var projectIssues = await totalIssues.Skip((request.PageNumber - 1) * request.PageSize)
                .Take(request.PageSize).ToListAsync(cancellationToken);

            foreach (var issue in projectIssues)
            {
                var responseModelIssue = _mapper.Map<GetAllIssuesQueryViewModel>(issue);

                if (!string.IsNullOrWhiteSpace(issue.AssignedTo))
                {
                    var user = _userManager.Users.SingleOrDefault(x => x.Id.ToLower() == issue.AssignedTo.ToLower());
                    responseModelIssue.Assignee = user?.Email;
                }

                responseModelIssue.Key = $"{issue?.Project?.Key}-{issue.Id}";
                responseModelIssue.CustomFieldInfo = GetCustomFieldInfo(issue.CustomFieldValues);
                responseModelIssue.Parent = issue?.Project?.Id.ToString();
                responseModelIssue.Type = issue.IssueType.Name;
                issues.Add(responseModelIssue);
            }

            return new PagedResponse<IEnumerable<GetAllIssuesQueryViewModel>>(issues, request.PageNumber,
                request.PageSize);
        }

        private static IEnumerable<CustomFieldInfo> GetCustomFieldInfo(ICollection<CustomFieldValue> issueCustomFieldValues)
        {
            List<CustomFieldInfo> customFieldInfo = null;

            if (issueCustomFieldValues.Any())
            {
                customFieldInfo = new List<CustomFieldInfo>();

                foreach (var issueCustomFieldValue in issueCustomFieldValues)
                {
                    customFieldInfo.Add(new CustomFieldInfo
                    {
                        Name = issueCustomFieldValue.CustomField.Name,
                        Value = issueCustomFieldValue.Value
                    });
                }
            }

            return customFieldInfo;
        }
    }
}
