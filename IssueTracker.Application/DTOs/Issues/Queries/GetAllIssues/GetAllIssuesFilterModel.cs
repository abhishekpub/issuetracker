﻿using IssueTracker.Application.Parameters;

namespace IssueTracker.Application.DTOs.Issues.Queries.GetAllIssues
{
    public class GetAllIssuesQueryParameter : RequestParameter
    {
        public string Assignee { get; set; }

        public int? ProjectId { get; set; }
    }
}
