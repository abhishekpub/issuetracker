﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Issues.Queries.GetAllIssues
{
    public class GetAllIssuesQueryValidator : BaseValidator<GetAllIssuesQuery>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected sealed override UserManager<ApplicationUser> UserManager { get; set; }

        public GetAllIssuesQueryValidator(IApplicationDbContext context, 
            IAuthenticatedUserService authenticatedUser,
            UserManager<ApplicationUser> userManager)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            UserManager = userManager;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.ProjectId)
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.")
                .MustAsync(UserIsInProjectParticipant).WithMessage("User does not belong to requested Project.");
        }

        private async Task<bool> ProjectShouldExists(int? projectId, CancellationToken cancellationToken)
        {
            if (projectId != null)
            {
                var project = await Context.Projects.FindAsync(projectId);
                return project != null;
            }

            return true;
        }

        private async Task<bool> UserIsInProjectParticipant(int? projectId, CancellationToken cancellationToken)
        {
            if (projectId != null)
            {
                var project = await Context.Projects.Include(x => x.ProjectParticipants)
                    .FirstOrDefaultAsync(x => x.Id == projectId, cancellationToken: cancellationToken);

                if (project != null)
                {
                    var requestedUser = UserManager.Users.SingleOrDefault(x => x.Id == AuthenticatedUser.UserId);

                    if (requestedUser != null)
                    {
                        if (project.ProjectParticipants.Any(x => x.UserId == requestedUser.Id))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            return true;
        }
    }
}
