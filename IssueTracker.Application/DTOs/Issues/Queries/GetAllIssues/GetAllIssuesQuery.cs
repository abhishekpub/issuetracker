﻿using System.Collections.Generic;
using IssueTracker.Application.DTOs.Account.Queries.GetAllUsers;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Issues.Queries.GetAllIssues
{
    public class GetAllIssuesQuery : IRequest<PagedResponse<IEnumerable<GetAllIssuesQueryViewModel>>>
    {
        public string Assignee { get; set; }
        public int? ProjectId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
