﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Issues.Command.UpdateIssue
{
    public class UpdateIssueCommandHandler : IRequestHandler<UpdateIssueCommand, Response<string>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IAuthenticatedUserService _authenticatedUser;
        private readonly IMapper _mapper;
        private UserManager<ApplicationUser> _userManager;

        public UpdateIssueCommandHandler(IApplicationDbContext context, IMapper mapper, IAuthenticatedUserService authenticatedUser, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _mapper = mapper;
            _authenticatedUser = authenticatedUser;
            _userManager = userManager;
        }

        public async Task<Response<string>> Handle(UpdateIssueCommand request, CancellationToken cancellationToken)
        {
            var issue = _context.Issues.Include(x => x.CustomFieldValues).Single(x => x.Id == request.Id);
            issue.Title = request.Title;
            issue.Description = request.Description;

            // Get db values for issue status, issue type and project
            if (request.IssueStatusId != 0)
            {
                var issueStatus = _context.IssueStatus.Single(x => x.Id == request.IssueStatusId);
                issue.IssueStatusId = issueStatus.Id;
            }

            if (!string.IsNullOrWhiteSpace(request.Assignee))
            {
                var user = _userManager.Users.Single(x => x.Email.ToLower() == request.Assignee.ToLower());
                issue.AssignedTo = user.Id;
            }

            var project = _context.Projects.Single(x => x.Id == issue.ProjectId);
            issue.Key = project.Key;
            UpdateCustomFieldValues(request, issue);

            _context.Issues.Update(issue);
            await _context.SaveChangesAsync(cancellationToken);

            return new Response<string>($"{project.Key}-{issue.Id}", "Issue updated successfully.");
        }

        private void UpdateCustomFieldValues(UpdateIssueCommand request, Issue issue)
        {
            if (request.CustomFieldInfo?.Any() == true)
            {
                // For each custom field check if that field present in db, if yes then update it otherwise create new field value record.
                foreach (var customFieldInfo in request.CustomFieldInfo)
                {
                    var dbCustomField =
                        _context.CustomFields.Single(x => x.Name.ToLower() == customFieldInfo.Name.ToLower());

                    if (dbCustomField != null)
                    {
                        var customFieldValue = issue.CustomFieldValues?.FirstOrDefault(x =>
                            x.CustomFieldId == dbCustomField.Id && x.IssueId == request.Id);

                        // if there is no value for the custom field then create a new record
                        if (customFieldValue == null)
                        {
                            issue.CustomFieldValues?.Add(new CustomFieldValue
                            {
                                CustomFieldId = dbCustomField.Id,
                                Value = customFieldInfo.Value
                            });
                        }
                        // otherwise update existing one.
                        else
                        {
                            customFieldValue.Value = customFieldInfo.Value;
                        }
                    }
                }
            }
        }
    }
}
