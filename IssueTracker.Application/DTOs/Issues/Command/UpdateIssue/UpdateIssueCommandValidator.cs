﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Issues.Command.UpdateIssue
{
    public class UpdateIssueCommandValidator : BaseValidator<UpdateIssueCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected sealed override UserManager<ApplicationUser> UserManager { get; set; }

        public UpdateIssueCommandValidator(IAuthenticatedUserService authenticatedUser, IApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            UserManager = userManager;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IssueShouldExists).WithMessage("{PropertyName} does not exists.");

            RuleFor(m => m)
                .Must(x => AssigneeShouldBeParticipantOfTheProject(x.Assignee, x.Id))
                .WithMessage("Assignee is not a participant of the project.")
                .WithName("Assignee");

            RuleFor(m => m.IssueStatusId)
                .MustAsync(IssueStatusShouldExists)
                .WithMessage("Invalid issue status.")
                .WithName("IssueStatusId");

            RuleFor(m => m)
                .Must(x => CustomFieldsShouldExists(x.CustomFieldInfo, x.Id))
                .WithMessage($"One or more invalid Custom field for requested issue type.")
                .Must(x => AreRequiredFieldsForIssueTypeArePresent(x.CustomFieldInfo, x.Id))
                .WithMessage("One or more required Custom field is missing from request.")
                .WithName("CustomFieldInfo");

            RuleFor(v => v.Description)
                .MaximumLength(500);
        }

        private bool CustomFieldsShouldExists(CustomFieldInfo[] customFieldsInfo, int issueId)
        {
            if (customFieldsInfo?.Any() == true)
            {
                var issue = Context.Issues.FirstOrDefault(x => x.Id == issueId);
                var dbCustomFields = Context.CustomFields.Where(x => x.IssueTypeId == issue.IssueTypeId);

                if (dbCustomFields.Any())
                {
                    var isAllFieldsPresent = true;
                    foreach (var customFieldInfo in customFieldsInfo)
                    {
                        if (dbCustomFields.Any(x => x.Name.ToLower() == customFieldInfo.Name.ToLower()))
                        {
                            continue;
                        }

                        isAllFieldsPresent = false;
                        break;
                    }
                    return isAllFieldsPresent;
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        private bool AreRequiredFieldsForIssueTypeArePresent(CustomFieldInfo[] customFieldsInfo, int issueId)
        {
            if (customFieldsInfo?.Any() == true)
            {
                var issue = Context.Issues.FirstOrDefault(x => x.Id == issueId);
                var dbIssueType = Context.IssueTypes.Include(x => x.CustomFields).FirstOrDefault(
                    x => x.Id == issue.IssueTypeId);

                if (dbIssueType != null)
                {
                    var dbRequiredCustomFieldNames = dbIssueType.CustomFields.Where(x => x.IsRequired)
                        .Select(x => x.Name.ToLower()).ToList();
                    if (dbRequiredCustomFieldNames.Any())
                    {
                        foreach (var customFieldInfo in customFieldsInfo)
                        {
                            // As soon as found that field is not present in db return false.
                            if (!dbRequiredCustomFieldNames.Contains(customFieldInfo.Name.ToLower()))
                            {
                                return false;
                            }

                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        private async Task<bool> IssueShouldExists(int issueId, CancellationToken cancellationToken)
        {
            var issue = await Context.Issues.SingleOrDefaultAsync(x => x.Id == issueId, cancellationToken);

            return issue != null;
        }

        private async Task<bool> IssueStatusShouldExists(int issueStatusId, CancellationToken cancellationToken)
        {
            if (issueStatusId == 0)
            {
                return true;
            }

            var issue = await Context.IssueStatus.SingleOrDefaultAsync(x => x.Id == issueStatusId, cancellationToken);
            return issue != null;
        }

        private bool AssigneeShouldBeParticipantOfTheProject(string userEmail, int issueId)
        {
            if (string.IsNullOrWhiteSpace(userEmail))
            {
                return true;
            }

            var user = UserManager.Users.SingleOrDefault(x => x.Email.ToLower() == userEmail.ToLower());
            var issue = Context.Issues.FirstOrDefault(x => x.Id == issueId);
            var project = Context.Projects.Include(x => x.ProjectParticipants).FirstOrDefault(x => x.Id == issue.ProjectId);

            if (user != null)
            {
                var projectParticipant = project?.ProjectParticipants.SingleOrDefault(x => x.UserId == user?.Id);
                if (projectParticipant != null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
