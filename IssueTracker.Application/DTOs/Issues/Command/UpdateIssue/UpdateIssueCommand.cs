﻿using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Issues.Command.UpdateIssue
{
    public class UpdateIssueCommand : IRequest<Response<string>>
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Assignee { get; set; }

        public int IssueStatusId { get; set; }

        public CustomFieldInfo[] CustomFieldInfo { get; set; }
    }
}
