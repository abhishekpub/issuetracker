﻿using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Issues.Command.CreateIssue
{
    public class CreateIssueCommand : IRequest<Response<string>>
    {
        public int ProjectId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Assignee { get; set; }

        public int IssueTypeId { get; set; }

        public CustomFieldInfo[] CustomFieldInfo { get; set; }
    }
}
