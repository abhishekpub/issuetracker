﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;
using MediatR;
using Microsoft.AspNetCore.Identity;
using IssueStatus = IssueTracker.Application.Enums.IssueStatus;

namespace IssueTracker.Application.DTOs.Issues.Command.CreateIssue
{
    public class CreateIssueCommandHandler : IRequestHandler<CreateIssueCommand, Response<string>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IAuthenticatedUserService _authenticatedUser;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public CreateIssueCommandHandler(IApplicationDbContext context, IMapper mapper, IAuthenticatedUserService authenticatedUser, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _mapper = mapper;
            _authenticatedUser = authenticatedUser;
            _userManager = userManager;
        }

        public async Task<Response<string>> Handle(CreateIssueCommand request, CancellationToken cancellationToken)
        {
            var issue = _mapper.Map<Issue>(request);

            // Get db values for issue status, issue type and project
            var issueStatus = _context.IssueStatus.Single(x => x.Name == IssueStatus.Todo.ToString());
            var issueType = _context.IssueTypes.Single(x => x.Id == request.IssueTypeId);
            var project = _context.Projects.Single(x => x.Id == request.ProjectId);
            var user = _userManager.Users.Single(x => x.Email.ToLower() == request.Assignee.ToLower());

            issue.IssueStatusId = issueStatus.Id;
            issue.IssueTypeId = issueType.Id;
            issue.Key = project.Key;
            issue.AssignedTo = user.Id;

            if (request.CustomFieldInfo?.Any() == true)
            {
                foreach (var customFieldInfo in request.CustomFieldInfo)
                {
                    var dbCustomField =
                        _context.CustomFields.Single(x => x.Name.ToLower() == customFieldInfo.Name.ToLower());

                    issue.CustomFieldValues = new List<CustomFieldValue>
                    {
                        new CustomFieldValue
                        {
                            CustomFieldId = dbCustomField.Id,
                            Value = customFieldInfo.Value,
                        }
                    };
                }
            }

            _context.Issues.Add(issue);
            await _context.SaveChangesAsync(cancellationToken);

            return new Response<string>($"{project.Key}-{issue.Id}", "Issue created successfully.");
        }
    }
}
