﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Models;
using IssueTracker.Application.Wrappers;

namespace IssueTracker.Application.DTOs.Issues.Command.CreateIssue
{
    public class CreateIssueCommandValidator : BaseValidator<CreateIssueCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected sealed override UserManager<ApplicationUser> UserManager { get; set; }

        public CreateIssueCommandValidator(IAuthenticatedUserService authenticatedUser, IApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            UserManager = userManager;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.ProjectId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.");

            RuleFor(v => v.IssueTypeId)
                .NotNull().WithMessage("{PropertyName} is required.")
                .NotEmpty();

            RuleFor(m => m)
                .Must(x => AssigneeShouldBeParticipantOfTheProject(x.Assignee, x.ProjectId))
                .WithMessage("Assignee is not a participant of the project.")
                .WithName("Assignee");

            RuleFor(m => m.IssueTypeId)
                .MustAsync(IssueTypeShouldExist)
                .WithMessage("Requested issue type not exists.");

            RuleFor(m => m)
                .Must(x => CustomFieldsShouldExists(x.CustomFieldInfo, x.IssueTypeId))
                .WithMessage($"One or more Custom field does not exists for requested issue type.")
                .Must(x => AreRequiredFieldsForIssueTypeArePresent(x.CustomFieldInfo, x.IssueTypeId))
                .WithMessage("One or more required Custom field is missing from request.")
                .WithName("CustomFieldInfo");

            RuleFor(v => v.Description)
                .MaximumLength(500);
        }

        private async Task<bool> IssueTypeShouldExist(int issueTypeId, CancellationToken cancellationToken)
        {
            var dbIssueType = await Context.IssueTypes.FirstOrDefaultAsync(
                x => x.Id == issueTypeId, cancellationToken: cancellationToken);

            return dbIssueType != null;
        }

        private bool CustomFieldsShouldExists(CustomFieldInfo[] customFieldsInfo, int issueTypeId)
        {
            if (customFieldsInfo == null || !customFieldsInfo.Any())
            {
                return true;
            }

            var dbCustomFields = Context.CustomFields.Where(x => x.IssueTypeId == issueTypeId);
            if (dbCustomFields.Any())
            {
                bool isAllFieldsPresent = true;
                foreach (var customFieldInfo in customFieldsInfo)
                {
                    if (dbCustomFields.Any(x => x.Name.ToLower() == customFieldInfo.Name.ToLower()))
                    {
                        continue;
                    }

                    isAllFieldsPresent = false;
                    break;
                }

                return isAllFieldsPresent;
            }

            return false;
        }

        private bool AreRequiredFieldsForIssueTypeArePresent(CustomFieldInfo[] customFieldsInfo, int issueTypeId)
        {
            var dbRequiredCustomFieldNames =
                Context.CustomFields.Include(x => x.IssueType)
                    .Where(x => x.IsRequired && x.IssueTypeId == issueTypeId)
                    .Select(x => x.Name.ToLower());

            if (dbRequiredCustomFieldNames?.Any() != true)
            {
                return true;
            }

            // If custom fields for the issue type is required but user does not provide.
            if (customFieldsInfo == null || customFieldsInfo?.Any() == false)
            {
                return false;
            }

            foreach (var customFieldInfo in customFieldsInfo)
            {
                // As soon as found that field is not present in db return false.
                if (!dbRequiredCustomFieldNames.Contains(customFieldInfo.Name.ToLower()))
                {
                    return false;
                }
            }

            return true;
        }

        protected bool AssigneeShouldBeParticipantOfTheProject(string userEmail, int projectId)
        {
            var user = UserManager.Users.SingleOrDefault(x => x.Email.ToLower() == userEmail.ToLower());
            var project = Context.Projects.Include(x => x.ProjectParticipants).FirstOrDefault(x => x.Id == projectId);

            if (user != null)
            {
                var projectParticipant = project?.ProjectParticipants.SingleOrDefault(x => x.UserId == user?.Id);
                if (projectParticipant != null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
