﻿using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Issues.Command.DeleteIssue
{
    public class DeleteIssueCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
    }
}
