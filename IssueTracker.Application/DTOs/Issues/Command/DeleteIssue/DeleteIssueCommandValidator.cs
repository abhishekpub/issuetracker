﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace IssueTracker.Application.DTOs.Issues.Command.DeleteIssue
{
    public class DeleteIssueCommandValidator : BaseValidator<DeleteIssueCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }
        protected sealed override UserManager<ApplicationUser> UserManager { get; set; }

        public DeleteIssueCommandValidator(IAuthenticatedUserService authenticatedUser, IApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            UserManager = userManager;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IssueShouldExist).WithMessage("{PropertyName} does not exists.")
                .MustAsync(CurrentUserShouldBeParticipantOfTheProject).WithMessage("User does not allowed to delete an issue.");
        }

        protected async Task<bool> CurrentUserShouldBeParticipantOfTheProject(int issueId, CancellationToken cancellationToken)
        {
            var issue = await Context.Issues.Include(x => x.Project).ThenInclude(x => x.ProjectParticipants)
                .FirstOrDefaultAsync(x => x.Id == issueId, cancellationToken: cancellationToken);

            var participant =
                issue?.Project.ProjectParticipants.FirstOrDefault(x => x.UserId == AuthenticatedUser.UserId);

            if (participant != null)
            {
                return true;
            }

            return false;
        }
    }
}
