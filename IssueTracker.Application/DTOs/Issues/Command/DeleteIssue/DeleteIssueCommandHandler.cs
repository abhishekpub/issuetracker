﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.Exceptions;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Issues.Command.DeleteIssue
{
    public class DeleteIssueCommandHandler : IRequestHandler<DeleteIssueCommand, Response<int>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IAuthenticatedUserService _authenticatedUser;
        private readonly IMapper _mapper;

        public DeleteIssueCommandHandler(IApplicationDbContext context, IMapper mapper, IAuthenticatedUserService authenticatedUser)
        {
            _context = context;
            _mapper = mapper;
            _authenticatedUser = authenticatedUser;
        }

        public async Task<Response<int>> Handle(DeleteIssueCommand request, CancellationToken cancellationToken)
        {
            var issue = _context.Issues
                .Include(x=>x.CustomFieldValues).
                SingleOrDefault(x => x.Id == request.Id);

            if (issue == null)
            {
                throw new ApiException("Issue not found.");
            }


            _context.Issues.Remove(issue);
            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(issue.Id, "Issue deleted successfully.");
        }
    }
}
