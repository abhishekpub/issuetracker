﻿using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Exceptions;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Queries.GetProjectById
{
    public class GetProjectByIdQueryHandler : IRequestHandler<GetProjectByIdQuery, Response<Domain.Entities.Project>>
    {
        private readonly IApplicationDbContext _applicationDbContext;

        public GetProjectByIdQueryHandler(IApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<Response<Domain.Entities.Project>> Handle(GetProjectByIdQuery request, CancellationToken cancellationToken)
        {
            var project = await _applicationDbContext.Projects.FindAsync(request.Id);

            if (project == null)
            {
                throw new ApiException($"Project Not Found.");
            }

            return new Response<Domain.Entities.Project>(project);
        }
    }
}
