﻿using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;

namespace IssueTracker.Application.DTOs.Project.Queries.GetProjectById
{
    public class GetProjectByIdQueryValidator : BaseValidator<GetProjectByIdQuery>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }

        public GetProjectByIdQueryValidator(IApplicationDbContext context, IAuthenticatedUserService authenticatedUser)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;

            RuleFor(v => v.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.");
        }
    }
}
