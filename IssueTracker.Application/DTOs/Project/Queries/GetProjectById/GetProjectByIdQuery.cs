﻿using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Queries.GetProjectById
{
    public class GetProjectByIdQuery : IRequest<Response<Domain.Entities.Project>>
    {
        public int Id { get; set; }
    }
}
