﻿using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.DTOs.Project.Commands.CreateProject
{
    public class CreateProjectCommandValidator : BaseValidator<CreateProjectCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }

        public CreateProjectCommandValidator(IAuthenticatedUserService authenticatedUser, IApplicationDbContext context)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            var regex = new Regex("^[A-Z]+$");
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.Key)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MinimumLength(3).WithMessage("{PropertyName} should not less than 3 characters.")
                .MaximumLength(4).WithMessage("{PropertyName} must not exceed 4 characters.")
                .Must(x => x.Split(" ").Length == 1).WithMessage("{PropertyName} should be one word long only.")
                .Must(x => regex.IsMatch(x)).WithMessage("{PropertyName} should be all in Upper Case and should only include alphabetic characters.")
                .MustAsync(IsProjectExists).WithMessage("{PropertyName} already exists.");

            RuleFor(v => v.Name)
                .MaximumLength(20).WithMessage("{PropertyName} must not exceed 20 characters.")
                .NotEmpty()
                .NotNull();

            RuleFor(v => v.Description)
                .MaximumLength(500);
        }

        private async Task<bool> IsProjectExists(string key, CancellationToken cancellationToken)
        {
            return await Context.Projects.AllAsync(x => x.Key != key, cancellationToken: cancellationToken);
        }
    }
}
