﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Commands.CreateProject
{
    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, Response<int>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IAuthenticatedUserService _authenticatedUser;
        private readonly IMapper _mapper;

        public CreateProjectCommandHandler(IApplicationDbContext context, IMapper mapper, IAuthenticatedUserService authenticatedUser)
        {
            _context = context;
            _mapper = mapper;
            _authenticatedUser = authenticatedUser;
        }

        public async Task<Response<int>> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = _mapper.Map<Domain.Entities.Project>(request);

            // Add user to participant list.

            project.ProjectParticipants = new List<Domain.Entities.ProjectParticipant>
            {
                new Domain.Entities.ProjectParticipant {UserId = _authenticatedUser.UserId}
            };

            _context.Projects.Add(project);
            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(project.Id, "Project created successfully.");
        }
    }
}
