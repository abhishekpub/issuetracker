﻿using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Commands.CreateProject
{
    public class CreateProjectCommand : IRequest<Response<int>>
    {
        public string Key { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
