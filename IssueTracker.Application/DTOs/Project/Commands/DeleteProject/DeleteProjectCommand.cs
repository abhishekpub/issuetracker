﻿using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Commands.DeleteProject
{
    public class DeleteProjectCommand : IRequest<Response<Unit>>
    {
        public int Id { get; set; }
    }
}
