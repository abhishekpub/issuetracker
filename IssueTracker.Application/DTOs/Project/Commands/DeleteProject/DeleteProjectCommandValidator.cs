﻿using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;

namespace IssueTracker.Application.DTOs.Project.Commands.DeleteProject
{
    public class DeleteProjectCommandValidator : BaseValidator<DeleteProjectCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }

        public DeleteProjectCommandValidator(IApplicationDbContext context, IAuthenticatedUserService authenticatedUser)
        {
            Context = context;
            AuthenticatedUser = authenticatedUser;
            CascadeMode = CascadeMode.Stop;

            RuleFor(v => v.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ShouldOwnerOfProject).WithMessage("User is not allowed to delete the project.");
        }
    }
}
