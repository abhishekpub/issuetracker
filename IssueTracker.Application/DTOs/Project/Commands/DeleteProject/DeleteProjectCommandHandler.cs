﻿using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Exceptions;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Commands.DeleteProject
{
    public class DeleteProjectCommandHandler : IRequestHandler<DeleteProjectCommand, Response<Unit>>
    {
        private readonly IApplicationDbContext _context;

        public DeleteProjectCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Response<Unit>> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await _context.Projects.FindAsync(request.Id);

            if (project == null)
            {
                throw new ApiException("Project Not Found.");
            }

            _context.Projects.Remove(project);

            await _context.SaveChangesAsync(cancellationToken);
            return new Response<Unit>(Unit.Value, "Project deleted successfully.");
        }
    }
}
