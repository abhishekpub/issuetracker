﻿using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Commands.UpdateProject
{
    public class UpdateProjectCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
