﻿using FluentValidation;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;

namespace IssueTracker.Application.DTOs.Project.Commands.UpdateProject
{
    public class UpdateProjectCommandValidator : BaseValidator<UpdateProjectCommand>
    {
        protected sealed override IApplicationDbContext Context { get; set; }
        protected sealed override IAuthenticatedUserService AuthenticatedUser { get; set; }

        public UpdateProjectCommandValidator(IApplicationDbContext context, IAuthenticatedUserService authenticatedUser)
        {
            CascadeMode = CascadeMode.Stop;
            Context = context;
            AuthenticatedUser = authenticatedUser;

            RuleFor(v => v.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(ProjectShouldExists).WithMessage("{PropertyName} does not exists.")
                .MustAsync(ShouldOwnerOfProject).WithMessage("User is not allowed to delete the project.");

            RuleFor(v => v.Name)
                .MaximumLength(50)
                .NotEmpty()
                .NotNull();

            RuleFor(v => v.Description)
                .MaximumLength(500);
        }
    }
}
