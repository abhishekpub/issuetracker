﻿using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Exceptions;
using IssueTracker.Application.Interfaces;
using IssueTracker.Application.Wrappers;
using MediatR;

namespace IssueTracker.Application.DTOs.Project.Commands.UpdateProject
{
    public class UpdateProjectCommandHandler : IRequestHandler<UpdateProjectCommand, Response<int>>
    {
        private readonly IApplicationDbContext _context;

        public UpdateProjectCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Response<int>> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await _context.Projects.FindAsync(request.Id);

            if (project == null)
            {
                throw new ApiException($"Project Not Found.");
            }

            project.Name = request.Name;
            project.Description = request.Description;

            _context.Projects.Update(project);
            await _context.SaveChangesAsync(cancellationToken);
            return new Response<int>(project.Id, "Project details updated successfully.");
        }
    }
}
