﻿using System.Threading.Tasks;
using IssueTracker.Application.DTOs.Email;

namespace IssueTracker.Application.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequest request);
    }
}
