﻿using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace IssueTracker.Application.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Project> Projects { get; set; }

        DbSet<ProjectParticipant> ProjectParticipants { get; set; }

        DbSet<CustomField> CustomFields { get; set; }

        DbSet<CustomFieldValue> CustomFieldValues { get; set; }

        DbSet<Issue> Issues { get; set; }

        DbSet<IssueStatus> IssueStatus { get; set; }

        DbSet<IssueType> IssueTypes { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}