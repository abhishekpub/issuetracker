﻿namespace IssueTracker.Application.Models
{
    public class CustomFieldInfo
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}