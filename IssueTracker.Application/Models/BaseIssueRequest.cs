﻿using IssueTracker.Application.Enums;

namespace IssueTracker.Application.Models
{
    public class BaseIssueRequest
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Reporter { get; set; }

        public string Assignee { get; set; }

        public IssueType Status { get; set; }
    }
}
