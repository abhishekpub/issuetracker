﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Interfaces;
using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;

namespace IssueTracker.Infrastructure.Persistence.Seeds
{
    public static class IssueType
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager, IApplicationDbContext context)
        {
            var defaultUser = userManager.Users.FirstOrDefault();
            if (!context.IssueTypes.Any())
            {
                var issueTypes = new List<Domain.Entities.IssueType>
                {
                    new Domain.Entities.IssueType
                    {
                        Name = "Task",
                        Description = "Task",
                        CreatedBy = defaultUser?.Id
                    },
                    new Domain.Entities.IssueType
                    {
                        Name = "Story",
                        Description = "Story",
                        CustomFields = new List<CustomField>
                        {
                            new CustomField
                            {
                                Description = "Story Points",
                                Name = "Story Points",
                                CreatedBy = defaultUser?.Id,
                                IsRequired = false
                            }
                        }
                    },
                    new Domain.Entities.IssueType
                    {
                        Name = "Bug",
                        Description = "Bug",
                        CustomFields = new List<CustomField>
                        {
                            new CustomField
                            {
                                Description = "Steps to replicate",
                                Name = "Steps to replicate",
                                CreatedBy = defaultUser?.Id,
                                IsRequired = false
                            }
                        }
                    }
                };

                await context.IssueTypes.AddRangeAsync(issueTypes);
                await context.SaveChangesAsync(CancellationToken.None);
            }
        }
    }
}
