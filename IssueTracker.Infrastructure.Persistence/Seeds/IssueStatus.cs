﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IssueTracker.Application.Interfaces;
using IssueTracker.Domain.Entities.Account;
using Microsoft.AspNetCore.Identity;

namespace IssueTracker.Infrastructure.Persistence.Seeds
{
    public static class IssueStatus
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager, IApplicationDbContext context)
        {
            var defaultUser = userManager.Users.FirstOrDefault();
            if (!context.IssueStatus.Any())
            {
                var issueStatus = new List<Domain.Entities.IssueStatus>
                {
                    new Domain.Entities.IssueStatus
                    {
                        Name = "ToDo",
                        Description = "ToDo Status",
                        CreatedBy = defaultUser?.Id
                    },
                    new Domain.Entities.IssueStatus
                    {
                        Name = "InProgress",
                        Description = "InProgress Status",
                        CreatedBy = defaultUser?.Id
                    },
                    new Domain.Entities.IssueStatus
                    {
                        Name = "Done",
                        Description = "Done Status",
                        CreatedBy = defaultUser?.Id
                    }
                };

                await context.IssueStatus.AddRangeAsync(issueStatus);
                await context.SaveChangesAsync(CancellationToken.None);
            }
        }
    }
}
