﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class IdentityUserLoginConfiguration : IEntityTypeConfiguration<IdentityUserLogin<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserLogin<string>> builder)
        {
            builder.Property<string>("LoginProvider")
                .HasColumnType("nvarchar(450)");

            builder.Property<string>("ProviderKey")
                .HasColumnType("nvarchar(450)");

            builder.Property<string>("ProviderDisplayName")
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("UserId")
                .IsRequired()
                .HasColumnType("nvarchar(450)");

            builder.HasKey("LoginProvider", "ProviderKey");

            builder.HasIndex("UserId");

            builder.HasOne("IssueTracker.Domain.Entities.Account.ApplicationUser", null)
                .WithMany()
                .HasForeignKey("UserId")
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.ToTable("UserLogins", "Identity");
        }
    }
}
