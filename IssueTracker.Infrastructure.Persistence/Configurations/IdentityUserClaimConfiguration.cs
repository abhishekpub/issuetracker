﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class IdentityUserClaimConfiguration : IEntityTypeConfiguration<IdentityUserClaim<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserClaim<string>> builder)
        {
            builder.Property<int>("Id")
                .ValueGeneratedOnAdd()
                .HasColumnType("int")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            builder.Property<string>("ClaimType")
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("ClaimValue")
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("UserId")
                .IsRequired()
                .HasColumnType("nvarchar(450)");

            builder.HasKey("Id");

            builder.HasIndex("UserId");

            builder.HasOne("IssueTracker.Domain.Entities.Account.ApplicationUser", null)
                .WithMany()
                .HasForeignKey("UserId")
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.ToTable("UserClaims", "Identity");
        }
    }
}
