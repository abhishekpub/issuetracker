﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class IdentityUserTokenConfiguration : IEntityTypeConfiguration<IdentityUserToken<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserToken<string>> builder)
        {
            builder.Property<string>("UserId")
                .HasColumnType("nvarchar(450)");

            builder.Property<string>("LoginProvider")
                .HasColumnType("nvarchar(450)");

            builder.Property<string>("Name")
                .HasColumnType("nvarchar(450)");

            builder.Property<string>("Value")
                .HasColumnType("nvarchar(max)");

            builder.HasKey("UserId", "LoginProvider", "Name");

            builder.HasOne("IssueTracker.Domain.Entities.Account.ApplicationUser", null)
                .WithMany()
                .HasForeignKey("UserId")
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.ToTable("UserTokens", "Identity");
        }
    }
}
