﻿using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class ProjectConfiguration : BaseEntityConfiguration<Project>
    {
        public new void Configure(EntityTypeBuilder<Project> builder)
        {
            base.Configure(builder);

            builder.Property<string>(x=> x.Key)
                .HasColumnType("nvarchar(4)")
                .HasMaxLength(4);

            builder.HasAlternateKey(x => x.Key);

            builder.Property<string>(x => x.Name)
                .HasColumnType("nvarchar(max)");

            builder.Property<string>(x => x.Description)
                .HasColumnType("nvarchar(max)");

            builder.HasMany(x => x.ProjectParticipants)
                .WithOne(x => x.Project);

            builder.ToTable("Projects");
        }
    }
}
