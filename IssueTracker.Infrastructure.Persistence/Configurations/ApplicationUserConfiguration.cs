﻿using System;
using IssueTracker.Domain.Entities.Account;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

            builder.Property<int>("AccessFailedCount")
                .HasColumnType("int");

            builder.Property<string>("ConcurrencyStamp")
                .IsConcurrencyToken()
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("Email")
                .HasColumnType("nvarchar(256)")
                .HasMaxLength(256);

            builder.Property<bool>("EmailConfirmed")
                .HasColumnType("bit");

            builder.Property<string>("FirstName")
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("LastName")
                .HasColumnType("nvarchar(max)");

            builder.Property<bool>("LockoutEnabled")
                .HasColumnType("bit");

            builder.Property<DateTimeOffset?>("LockoutEnd")
                .HasColumnType("datetimeoffset");

            builder.Property<string>("NormalizedEmail")
                .HasColumnType("nvarchar(256)")
                .HasMaxLength(256);

            builder.Property<string>("NormalizedUserName")
                .HasColumnType("nvarchar(256)")
                .HasMaxLength(256);

            builder.Property<string>("PasswordHash")
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("PhoneNumber")
                .HasColumnType("nvarchar(max)");

            builder.Property<bool>("PhoneNumberConfirmed")
                .HasColumnType("bit");

            builder.Property<string>("SecurityStamp")
                .HasColumnType("nvarchar(max)");

            builder.Property<bool>("TwoFactorEnabled")
                .HasColumnType("bit");

            builder.Property<string>("UserName")
                .HasColumnType("nvarchar(256)")
                .HasMaxLength(256);

            builder.HasKey("Id");

            builder.HasIndex("NormalizedEmail")
                .HasName("EmailIndex");

            builder.HasIndex("NormalizedUserName")
                .IsUnique()
                .HasName("UserNameIndex")
                .HasFilter("[NormalizedUserName] IS NOT NULL");

            builder.ToTable("User","Identity");
        }
    }
}
