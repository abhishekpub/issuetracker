﻿using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class CustomFieldValueConfiguration : BaseEntityConfiguration<CustomFieldValue>
    {
        public new void Configure(EntityTypeBuilder<CustomFieldValue> builder)
        {
            base.Configure(builder);
            
            builder.Property<string>(x => x.Value)
                .HasColumnType("nvarchar(max)")
                .IsRequired();

            builder.HasOne<Issue>(x => x.Issue)
                .WithMany(x => x.CustomFieldValues)
                .HasForeignKey(x => x.IssueId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.HasOne<CustomField>(x => x.CustomField)
                .WithMany(x => x.CustomFieldValues)
                .HasForeignKey(x => x.CustomFieldId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired();

            builder.ToTable("CustomFieldValues");
        }
    }
}
