﻿using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class CustomFieldConfiguration : BaseEntityConfiguration<CustomField>
    {
        public new void Configure(EntityTypeBuilder<CustomField> builder)
        {
            base.Configure(builder);

            builder.Property<string>(x => x.Name)
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.HasAlternateKey(x => new {x.Name, x.IssueTypeId});

            builder.Property<string>(x => x.Description)
                .HasColumnType("nvarchar(max)");

            builder.Property<string>(x => x.DefaultValue)
                .HasColumnType("nvarchar(max)");

            builder.HasMany(x => x.CustomFieldValues)
                .WithOne(x => x.CustomField);

            builder.HasOne<IssueType>(x => x.IssueType)
                .WithMany(x => x.CustomFields)
                .HasForeignKey(x => x.IssueTypeId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.ToTable("CustomFields");
        }
    }
}
