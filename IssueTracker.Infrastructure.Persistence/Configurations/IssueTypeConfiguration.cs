﻿using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class IssueTypeConfiguration : BaseEntityConfiguration<IssueType>
    {
        public new void Configure(EntityTypeBuilder<IssueType> builder)
        {
            base.Configure(builder);

            builder.Property<string>(x => x.Name)
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.HasAlternateKey(x=>x.Name);

            builder.Property<string>(x => x.Description)
                .HasColumnType("nvarchar(max)");

            builder.HasMany(x => x.Issues)
                .WithOne(x => x.IssueType);

            builder.HasMany(x => x.CustomFields)
                .WithOne(x => x.IssueType);

            builder.ToTable("IssueTypes");
        }
    }
}
