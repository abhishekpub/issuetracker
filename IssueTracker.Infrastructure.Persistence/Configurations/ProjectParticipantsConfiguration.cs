﻿using IssueTracker.Domain.Entities;
using IssueTracker.Domain.Entities.Account;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class ProjectParticipantsConfiguration : BaseEntityConfiguration<ProjectParticipant>
    {
        public new void Configure(EntityTypeBuilder<ProjectParticipant> builder)
        {
            base.Configure(builder);

            builder.HasAlternateKey("ProjectId", "UserId");

            builder.HasOne<ApplicationUser>(x=>x.ApplicationUser)
                .WithMany()
                .HasForeignKey(x=>x.UserId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.HasOne<Project>(x=> x.Project)
                .WithMany(x=>x.ProjectParticipants)
                .HasForeignKey(x=>x.ProjectId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.ToTable("ProjectParticipants");
        }
    }
}
