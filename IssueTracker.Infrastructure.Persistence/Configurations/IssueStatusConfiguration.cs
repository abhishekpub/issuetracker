﻿using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class IssueStatusConfiguration : BaseEntityConfiguration<IssueStatus>
    {
        public new void Configure(EntityTypeBuilder<IssueStatus> builder)
        {
            base.Configure(builder);

            builder.Property<string>(x => x.Name)
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property<string>(x => x.Description)
                .HasColumnType("nvarchar(max)");

            builder.HasMany(x => x.Issues)
                .WithOne(x => x.Status);

            builder.ToTable("IssueStatus");
        }
    }
}
