﻿using System;
using IssueTracker.Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : AuditableBaseEntity
    {
        public void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property<int>(x => x.Id)
                .ValueGeneratedOnAdd()
                .HasColumnType("int")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            builder.Property<DateTime>(x => x.Created)
                .HasColumnType("datetime2");

            builder.Property<string>(x => x.CreatedBy)
                .HasColumnType("nvarchar(max)");

            builder.Property<DateTime?>(x => x.LastModified)
                .HasColumnType("datetime2");

            builder.Property<string>(x => x.LastModifiedBy)
                .HasColumnType("nvarchar(max)");

            builder.HasKey(x => x.Id);
        }
    }
}
