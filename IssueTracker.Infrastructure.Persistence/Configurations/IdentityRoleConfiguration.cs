﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    class IdentityRoleConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.Property<string>("Id")
                .HasColumnType("nvarchar(450)");

            builder.Property<string>("ConcurrencyStamp")
                .IsConcurrencyToken()
                .HasColumnType("nvarchar(max)");

            builder.Property<string>("Name")
                .HasColumnType("nvarchar(256)")
                .HasMaxLength(256);

            builder.Property<string>("NormalizedName")
                .HasColumnType("nvarchar(256)")
                .HasMaxLength(256);

            builder.HasKey("Id");

            builder.HasIndex("NormalizedName")
                .IsUnique()
                .HasName("RoleNameIndex")
                .HasFilter("[NormalizedName] IS NOT NULL");

            builder.ToTable("Role", "Identity");
        }
    }
}
