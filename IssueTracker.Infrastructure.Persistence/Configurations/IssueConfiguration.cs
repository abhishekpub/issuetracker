﻿using IssueTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IssueTracker.Infrastructure.Persistence.Configurations
{
    public class IssueConfiguration : BaseEntityConfiguration<Issue>
    {
        public new void Configure(EntityTypeBuilder<Issue> builder)
        {
            base.Configure(builder);

            builder.Property<string>(x => x.Key)
                .HasColumnType("nvarchar(10)")
                .HasMaxLength(10);

            builder.HasAlternateKey(x => x.Key);

            builder.Property<string>(x => x.Description)
                .HasColumnType("nvarchar(max)");

            builder.HasMany(x => x.CustomFieldValues)
                .WithOne(x => x.Issue);

            builder.HasOne<IssueType>(x => x.IssueType)
                .WithMany(x => x.Issues)
                .HasForeignKey(x => x.IssueTypeId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.HasOne<Project>(x => x.Project)
                .WithMany(x => x.Issues)
                .HasForeignKey(x => x.ProjectId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.HasOne<IssueStatus>(x => x.Status)
                .WithMany(x => x.Issues)
                .HasForeignKey(x => x.IssueStatusId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder.ToTable("Issues");
        }
    }
}
