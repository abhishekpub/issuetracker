﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IssueTracker.Infrastructure.Persistence.Migrations
{
    public partial class RenamedAssignedColumnFromIssueTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Assignee",
                table: "Issues");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo",
                table: "Issues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedTo",
                table: "Issues");

            migrationBuilder.AddColumn<string>(
                name: "Assignee",
                table: "Issues",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
