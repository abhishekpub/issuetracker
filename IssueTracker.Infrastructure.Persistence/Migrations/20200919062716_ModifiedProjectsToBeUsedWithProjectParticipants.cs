﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IssueTracker.Infrastructure.Persistence.Migrations
{
    public partial class ModifiedProjectsToBeUsedWithProjectParticipants : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Projects_Key",
                table: "Projects");

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(4)",
                oldMaxLength: 4);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Key",
                table: "Projects",
                type: "nvarchar(4)",
                maxLength: 4,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Projects_Key",
                table: "Projects",
                column: "Key");
        }
    }
}
