## Introduction

This is simple API endpoints for mini kind of JIRA application, which exposes endpoints -
- Projects
- Project Participants
- Issues
- Account

# Prerequisites
- Visual Studio 2019 Community and above
- .NET Core 3.1 SDK and above
- Basic Understanding of Architectures and Clean Code Principles

# Local setup
- Clone the repository.
- open up [WebAPI/appsettings.json](https://gitlab.com/abhishekpub/issuetracker/-/blob/develop/IssueTracker.WebApi/appsettings.json) to change the connection strings.
- Once that is Set, Run below commands to update the database (ensure nuget package manager should point to project - IssueTracker.Infrastructure.Persistence).
    - `update-database -Context ApplicationDbContext`

Finally, build and run the Application.

Default Roles & Credentials
As soon you build and run your application, default users and roles get added to the database.

## Default Roles are as follows.

- SuperAdmin
- Admin
- User

Here are the credentials for the default users.

- Email - admin@gmail.com / Password - Password@123
- Email - superadmin@gmail.com / Password - Password@123
- Email - user@gmail.com / Password - Password@123

You can use these default credentials to generate valid JWTokens at the ../api/account/authenticate endpoint.

PS - **If it brings up a dialog box saying 'Site could not be reached', this is probably a configuration issue with the Local IIS. You can switch to Kestrel server and run the application.**

This usually happens when IIS faces issues. You can switch to kestrel and run the application.

## Technologies
- ASP.NET Core 3.1 WebApi
- REST Standards
- .NET Core 3.1 / Standard 2.1 Libraries

- CQRS with MediatR Library
- Entity Framework Core - Code First
- Repository Pattern - Generic (POC)
- MediatR Pipeline Logging & Validation
- Swagger UI
- Response Wrappers
- Pagination
- Microsoft Identity with JWT Authentication
- Database Seeding
- Custom Exception Handling Middlewares
- API Versioning
- Fluent Validation
- Automapper
- Complete User Management Module (Register / Generate Token / Forgot Password / Confirmation Mail)
    - Email functionality impleted but ot integrated in existing functionality.
- User Auditing