﻿using IssueTracker.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Security.Claims;

namespace IssueTracker.WebApi.Services
{
    public class AuthenticatedUserService : IAuthenticatedUserService
    {
        public AuthenticatedUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext?.User?.FindFirstValue("uid");
            //EmailId = httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(claim => claim.Type == JwtRegisteredClaimNames.Email)?.Value;
            //UserName = httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(claim => claim.Type == JwtRegisteredClaimNames.Sub)?.Value;
            //var tt = httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(claim => claim.Type == "roles")?.Value;
        }

        public string UserId { get; }
        public string UserName { get; }
        public string EmailId { get; }
        public List<string> Roles { get; }
    }
}
