﻿using System.Threading.Tasks;
using IssueTracker.Application.DTOs.ProjectParticipant.Commands.CreateProjectParticipant;
using IssueTracker.Application.DTOs.ProjectParticipant.Commands.DeleteProjectParticipant;
using IssueTracker.Application.DTOs.ProjectParticipant.Queries.GetAllProjectParticipants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IssueTracker.WebApi.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectParticipantController : BaseApiController
    {
        /// <summary>
        /// Create new project participant.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateProjectParticipantCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Get participants from the request project.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetProjectParticipantsByProjectIdQuery filter)
        {
            return Ok(await Mediator.Send(new GetProjectParticipantsByProjectIdQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber, ProjectId = filter.ProjectId}));
        }

        /// <summary>
        /// Delete participant from the requested project.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> Delete(DeleteProjectParticipantCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
