﻿using System.Threading.Tasks;
using IssueTracker.Application.DTOs.Issues.Command.CreateIssue;
using IssueTracker.Application.DTOs.Issues.Command.DeleteIssue;
using IssueTracker.Application.DTOs.Issues.Command.UpdateIssue;
using IssueTracker.Application.DTOs.Issues.Queries.GetAllIssues;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IssueTracker.WebApi.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class IssueController : BaseApiController
    {
        /// <summary>
        /// Create new issue in existing project.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateIssueCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Get all issues from existing project.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("get-all-issues")]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] GetAllIssuesQueryParameter filter)
        {
            return Ok(await Mediator.Send(new GetAllIssuesQuery
            {
                ProjectId = filter.ProjectId,
                Assignee = filter.Assignee,
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize
            }));
        }

        /// <summary>
        /// Update the existing project issue.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, UpdateIssueCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Delete existing project issue.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteIssueCommand { Id = id }));
        }
    }
}
